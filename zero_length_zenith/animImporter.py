# -*- coding:utf-8 -*-

"""
This file is part of ZLZ.

ZLZ is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ZLZ is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with ZLZ.  If not, see <https://www.gnu.org/licenses/>
"""


"""
Handles importing object animations.

TODO:
- remove haxKeyframes once we have the new version of HSPlasma.
- figure out keyframe tangents
- import more keyframe types (quaternion, mostly).
- add more applicators (light animations, etc)
- animation events ?
- message forwarders
#"""

import bpy
from mathutils import *
import PyHSPlasma as pl
from zero_length_zenith.utils import *

class AnimImporter:
    
    def __init__(self, parent):
        self.parent = parent
        self.animationChannels = {} # maps a channel name to a scene object key
        self.masterModsToImport = [] # list of (sceneObjKey, agMasterModKey)
        self.animHolder = None
        self.version = None
    
    def importSimpleController(self, controller, blAction, curveName):
        keyframes = controller.keys[0]
        frameMultiplier = bpy.context.scene.render.fps / bpy.context.scene.render.fps_base / 30
        blFCurveX = blAction.fcurves.new(curveName, 0)
        blFCurveY = blAction.fcurves.new(curveName, 1)
        blFCurveZ = blAction.fcurves.new(curveName, 2)
        for keyframe in keyframes:
            frame = keyframe.frame * frameMultiplier
            blKfX = blFCurveX.keyframe_points.insert(frame, keyframe.value.X)
            blKfY = blFCurveY.keyframe_points.insert(frame, keyframe.value.Y)
            blKfZ = blFCurveZ.keyframe_points.insert(frame, keyframe.value.Z)
            
            forceLinear = False
            if forceLinear:
                blKfX.handle_right = Vector((frame, keyframe.value.X))
                blKfX.handle_left = Vector((frame, keyframe.value.X))
                blKfY.handle_right = Vector((frame, keyframe.value.Y))
                blKfY.handle_left = Vector((frame, keyframe.value.Y))
                blKfZ.handle_right = Vector((frame, keyframe.value.Z))
                blKfZ.handle_left = Vector((frame, keyframe.value.Z))
            # else handles are already positioned correctly
            
            self.parent.endFrame = max(self.parent.endFrame, frame)
    
    def importCompoundController(self, controller, blAction, curveName):
        x = controller.X
        y = controller.Y
        z = controller.Z
        i=0
        frameMultiplier = bpy.context.scene.render.fps / bpy.context.scene.render.fps_base / 30
        for axisController in [x,y,z]:
            blFCurve = blAction.fcurves.new(curveName, i)
            for keyframe in axisController.keys[0]:
                frame = keyframe.frame * frameMultiplier
                blKf = blFCurve.keyframe_points.insert(frame, keyframe.value)
                
                forceLinear = False
                if forceLinear:
                    blKf.handle_right = Vector((frame, keyframe.value))
                    blKf.handle_left = Vector((frame, keyframe.value))
                # else handles are already positioned correctly
                
                self.parent.endFrame = max(self.parent.endFrame, frame)
            i += 1
    
    def importAnimations(self):
        dontAttachAnimationsToObjects = not self.parent.config["attachAnimationsToObjects"]
        
        # create the anim holder object. We'll use it for bones even if all other animations are attached to the correct object.
        self.animHolder = bpy.data.objects.new("animHolder", None)
        bpy.context.scene.objects.link(self.animHolder)
        lyrs = [False] * 20
        lyrs[-1] = True
        self.animHolder.layers = lyrs
        
        for sceneObjKey, masterModKey in self.masterModsToImport:
            if not self.parent.validKey(masterModKey):
                continue
            print("    Master %s" % sceneObjKey.name)
            masterMod = masterModKey.object
            for animKey in masterMod.privateAnims:
                if not self.parent.validKey(animKey):
                    continue
                isAtc = True
                if animKey.type != pl.plFactory.kATCAnim:
                    print("WARNING: animation type %d not fully supported yet !" % animKey.type)
                    isAtc = False
                anim = animKey.object
                self.version = animKey.location.version
                
                for applicator in anim.applicators:
                    if isinstance(applicator, pl.plMatrixChannelApplicator):
                        targetObjKey = self.animationChannels[applicator.channelName]
                        if dontAttachAnimationsToObjects:
                            blObj = self.animHolder
                        else:
                            if not targetObjKey in self.parent.drawImporter.bonesToReconstruct:
                                blObj = self.parent.getBlObjectFromKey(targetObjKey)
                            else:
                                print("WARNING - no animation for bones yet... Attaching to anim holder instead of object %s" % targetObjKey.name)
                                blObj = self.animHolder
                        blAnimationData = blObj.animation_data
                        if not blAnimationData:
                            blAnimationData = blObj.animation_data_create()
                        
                        print("        %s, on object %s" % (animKey.name, targetObjKey.name))
                        blAction = bpy.data.actions.new(anim.name)
                        blAction.use_fake_user = True # just in case...
                        blAnimationData.action = blAction # this is overwritten by each new animation channel, but should be enough to ensure actions are pushed on the object's nla stack...
                        
                        channel = applicator.channel
                        controller = channel.controller
                        
                        affine = channel.affine
                        if affine:
                            """
                            Mmhkay, this is important !
                            When we import objects, we let Blender figure out pos/rot/scale from the coordinate interface's matrix.
                            This works, but Blender only picks ONE of several valid combinations - not always the correct one...
                            Rotation of 180°, for instance, is also equivalent to a non-uniform negative scaling...
                            And, as you would expect, Cyan's artists never learnt that you should NEVER EVER scale objects if you can avoid it.
                            So they like to leave objects with non-one, non-uniform, negative or partially negative scaling. Booo !
                            
                            Fortunately, this is usually not a problem for regular objects. This is however an issue for some animated objects,
                            like the butterflies in Relto. Those objects have rotation animated, but not scale. This causes issues because
                            rotation and scale are supposed to work together.
                            Solution: animated objects are repositioned manually using the affine parts of the animation.
                            Oh, and be grateful Blender has the exact same space as Plasma. Otherwise that would have been bloody.
                            
                            ...
                            (I have NO FRIGGIN IDEA how this is going to behave with parenting and the like. Let's assume we live
                            in a world of rainbows and sunshines, and the math will just fall in place correctly.)
                            #"""
                            blObj.location = Vector((affine.T.X, affine.T.Y, affine.T.Z))
                            blObj.rotation_quaternion = Quaternion((affine.Q.W, affine.Q.X, affine.Q.Y, affine.Q.Z))
                            blObj.scale = Vector((affine.K.X, affine.K.Y, affine.K.Z))
                            
                            if affine.F < 0:
                                # We have at least one component of the scale vector which is negative.
                                # Hmmm... Simply assume we have to negate the whole scale.
                                # This appears to be working.
                                blObj.scale = -blObj.scale
                        
                        pos, rot, sca = None, None, None
                        if isinstance(controller, pl.plTMController):
                            pos, rot, sca = controller.pos, controller.rot, controller.scale
                        elif isinstance(controller, pl.plCompoundController):
                            pos, rot, sca = controller.X, controller.Y, controller.Z
                        else:
                            print("WARNING: animation controller %s not supported!" % controller.__class__.__name__)
                            continue
                        
                        if pos:
                            if isinstance(pos, pl.plSimplePosController):
                                self.importSimpleController(pos.position, blAction, "location")
                            elif isinstance(pos, (pl.plSimplePosController, pl.plLeafController)):
                                self.importSimpleController(pos, blAction, "location")
                            elif isinstance(pos, (pl.plCompoundPosController, pl.plCompoundController)):
                                self.importCompoundController(pos, blAction, "location")
                            else:
                                print("WARNING: animation pos controller %s not supported !" % pos.__class__.__name__)
                        if rot:
                            if isinstance(rot, pl.plSimpleRotController):
                                self.importSimpleController(rot.rot, blAction, "rotation_euler")
                            elif isinstance(rot, pl.plLeafController):
                                self.importSimpleController(rot, blAction, "rotation_euler")
                            elif isinstance(rot, (pl.plCompoundRotController, pl.plCompoundController)):
                                self.importCompoundController(rot, blAction, "rotation_euler")
                            else:
                                print("WARNING: animation rot controller %s not supported !" % rot.__class__.__name__)
                        if sca:
                            # note: hsScaleKeys also contain a quaternion, which (if I'm correct) is used
                            # to define the space (orientation) the scaling is performed in. This space may NOT match the object's own rotation.
                            # We are NOT going to import this, period. That would be hell to deal with.
                            # I don't even know why anyone would ever allow non-local scaling in a game engine...
                            if isinstance(sca, pl.plSimpleScaleController):
                                self.importSimpleController(sca.value, blAction, "scale")
                            elif isinstance(sca, pl.plLeafController):
                                self.importSimpleController(sca, blAction, "scale")
                            else:
                                print("WARNING: animation scale controller %s not supported !" % sca.__class__.__name__)
