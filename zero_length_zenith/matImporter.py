# -*- coding:utf-8 -*-

"""
This file is part of ZLZ.

ZLZ is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ZLZ is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with ZLZ.  If not, see <https://www.gnu.org/licenses/>
"""


"""
Handles importing materials and layers, and attempts to fix all the crazy issues of importing Plasma materials on Blender.
Texture importing is done by imageImporter.
#"""

import bpy
from mathutils import *
from math import *
import PyHSPlasma as pl
from zero_length_zenith.utils import *
import json

class MatImporter:
    
    def __init__(self, parent):
        self.parent = parent
        self.knownMaterials = {}
        self.plLayerToBlTex = {}
        self.lightmappedMaterials = {} # material: lightmap resolution
    
    def getBaseLayerKey(self, plLayerKey):
        plLayer = plLayerKey.object
        if plLayer and self.parent.validKey(plLayer.underLay):
            return self.getBaseLayerKey(plLayer.underLay)
        return plLayerKey
    
    def isPlLayerVisibleAndSurface(self, plLayerKey):
        if not self.parent.validKey(plLayerKey):
            return False
        plLayer = plLayerKey.object
        if hasFlags(plLayer.state.blendFlags, pl.hsGMatState.kBlendNoTexColor):
            return False
        if not self.parent.validKey(plLayer.texture):
            return True
        if plLayer.texture.type in (
                pl.plFactory.kCubicEnvironmap,
                pl.plFactory.kDynamicEnvMap,
                pl.plFactory.kDynamicCamMap):
            return False
        return True
    
    def isPlLayerMask(self, plLayerKey):
        if not self.parent.validKey(plLayerKey):
            return False
        plLayer = plLayerKey.object
        return hasFlags(plLayer.state.blendFlags, pl.hsGMatState.kBlendAlpha) \
            and hasFlags(plLayer.state.blendFlags, pl.hsGMatState.kBlendAlphaMult) \
            and hasFlags(plLayer.state.blendFlags, pl.hsGMatState.kBlendNoTexColor)
    
    def importMaterial(self, plMatKey):
        blMat = self.knownMaterials.get(plMatKey)
        if blMat:
            return blMat
        
        plMat = plMatKey.object
        matName = stripIllegalChars(plMatKey.name)
        blMat = bpy.data.materials.new(matName)
        blMat.diffuse_intensity = 1
        blMat.specular_intensity = 0
        blMat.transparency_method = "Z_TRANSPARENCY"
        blMat.diffuse_color = Color((1,1,1))
        if not plMat:
            # no material ? Ack !
            blMat.diffuse_color = Color((0,.3,0))
        else:
            isDecal = hasFlags(plMat.compFlags, pl.hsGMaterial.kCompDecal)
            blMat.use_transparency = isDecal
            blMat.offset_z = 1 if isDecal else 0
            blMat.use_cast_shadows = not isDecal
            
            importProps = {}
            for i in range(len(plMat.layers)):
                importProps = self.importLayer(plMatKey, blMat, i, importProps)
            importProps2 = {}
            for i in range(len(plMat.piggyBacks)):
                importProps2 = self.importPiggyBack(plMatKey, blMat, i, importProps2)
            
            # NOTE:
            #   Materials which can't be imported (usually those using the kBindNext flag) will automatically be colored pink so user knows he has to fix it himself.
            #   Similarly, since UV rotation is not possible in Blender, layers which contain UV rotation will gain a custom property storing this rotation,
            #   and will be colored yellow. When it also can't be imported, it will be colored cyan.
            #   Baking the UV rotation into the mesh is possible, but too complex due to materials being shared by multiple meshes, UVs being shared by multiple layers, etc. So it's best left at the artist's discretion.
            if "materialIsInvalid" in importProps or "materialIsInvalid" in importProps2:
                blMat.diffuse_color = Color((1,0,1))
                if "hasInvalidRotation" in importProps or "hasInvalidRotation" in importProps2:
                    blMat.diffuse_color = Color((0,1,1))
            elif "hasInvalidRotation" in importProps or "hasInvalidRotation" in importProps2:
                blMat.diffuse_color = Color((1,1,0))
        
        self.knownMaterials[plMatKey] = blMat
        
        return blMat
    
    def importLayer(self, plMatKey, blMat, layerId, importProps):
        if "ignore" in importProps:
            if importProps["ignore"] > 0:
                importProps["ignore"] -= 1
                return importProps
        
        plMat = plMatKey.object
        lyrKey = self.getBaseLayerKey(plMat.layers[layerId]) # TODO import layer anim
        lyr = lyrKey.object
        lyrName = stripIllegalChars(lyrKey.name)
        
        existingTexture = self.plLayerToBlTex.get(lyrKey)
        # even if existingTexture is non null, re-import it over. This ensure the layer doesn't end up duplicated.
        
        preshade = (lyr.preshade.red, lyr.preshade.green, lyr.preshade.blue, lyr.preshade.alpha)
        runtime = (lyr.runtime.red, lyr.runtime.green, lyr.runtime.blue, lyr.runtime.alpha)
        preshadeIntensity = (preshade[0] + preshade[1] + preshade[2]) / 3 * preshade[3]
        runtimeIntensity = (runtime[0] + runtime[1] + runtime[2]) / 3 * runtime[3]
        finalColor = preshade
        if runtimeIntensity > preshadeIntensity:
            finalColor = runtime
        
        firstColorLayerSet = "firstColorLayerSet" in importProps
        
        if lyr.texture and lyr.texture.object:
            lyrTexKey = lyr.texture
            lyrTex = lyrTexKey.object
            
            if lyrTexKey.type == pl.plFactory.kDynamicTextMap:
                # no way we can import that...
                # just add an empty layer
                slot = blMat.texture_slots.add()
                if existingTexture:
                    texture = existingTexture
                else:
                    texture = bpy.data.textures.new(lyrName, "NONE")
                self.plLayerToBlTex[lyrKey] = texture
                slot.texture = texture
                slot.use_map_color_diffuse = False
                slot.color = Color((finalColor[0], finalColor[1], finalColor[2]))
            elif lyrTexKey.type == pl.plFactory.kLODMipmap:
                # this is just a regular mipmap... but HSPlasma has no bindings for it. Dagn !
                # let's import nothing.
                slot = blMat.texture_slots.add()
                if existingTexture:
                    texture = existingTexture
                else:
                    texture = bpy.data.textures.new(lyrName, "NONE")
                self.plLayerToBlTex[lyrKey] = texture
                slot.texture = texture
                slot.use_map_color_diffuse = False
                slot.color = Color((finalColor[0], finalColor[1], finalColor[2]))
                if not firstColorLayerSet:
                    blMat.diffuse_color = slot.color
                    importProps["firstColorLayerSet"] = True
            elif lyrTexKey.type == pl.plFactory.kMipmap:
                if hasFlags(lyr.state.miscFlags, (pl.hsGMatState.kMiscBumpDu | pl.hsGMatState.kMiscBumpDv | pl.hsGMatState.kMiscBumpDw)):
                    # Plasma hacky normalmapping. Ignore.
                    return importProps
                if hasFlags(lyr.state.miscFlags, pl.hsGMatState.kMiscBindNext):
                    # OHFUCKOHFUCK
                    # BindNext is the "plazma overpowerz" flags. Basically, it means whatever you do, you have a 90% probability to be unable to import the material correctly
                    # It's used a lot to combine alpha of two textures, and do stenciling. Both of which are usually incompatible with Blender's way of doing things.
                    importProps["materialIsInvalid"] = True # well, that's all we can do: let the user figure it out.
                    if layerId + 1 < len(plMat.layers):
                        nextLyrKey = plMat.layers[layerId+1]
                        if nextLyrKey.object and nextLyrKey.object.texture and nextLyrKey.object.texture.name.find("ALPHA_BLEND_FILTER") != -1 and firstColorLayerSet:
                            # ... well, OKAY, let's at least import basic stencils... But even these might look completely wrong !
                            # Import the stencil before this layer, as that's how Blender handles stuff.
                            importProps["isStencil"] = True # the value is never verified as having the key in the dict is enough. For readability, set it to true nonetheless
                            self.importLayer(plMatKey, blMat, layerId+1, importProps)
                            importProps.pop("isStencil", None)
                            importProps["ignore"] = 1 # ignore importing the next layer
                slot = blMat.texture_slots.add()
                slot.color = Color((finalColor[0], finalColor[1], finalColor[2]))
                if existingTexture:
                    texture = existingTexture
                else:
                    texture = bpy.data.textures.new(lyrName, "IMAGE")
                self.plLayerToBlTex[lyrKey] = texture
                slot.texture = texture
                texture.image = self.parent.imageImporter.getImage(lyrTexKey)
                opacity = lyr.opacity
                
                plMtx = lyr.transform.mat
                mat = Matrix(plMtx)
                loc, rot, sca = mat.decompose() # TODO handle viewport-computed uvs
                # Blender scales the texture from its center (user-friendly version),
                # Plasma from its corner (programmer-friendly version)
                slot.offset = [(sca.x-1)/2 + loc.x, (sca.y-1)/2 + loc.y, (sca.z-1)/2 + loc.z]
                slot.offset.y *= -1 # DX vs OGL
                slot.scale = sca
                rot = rot.to_euler().z
                if rot:
                    print("        Layer %s: not importing rotation" % lyrName)
                    importProps["hasInvalidRotation"] = True
                    texture["rotation"] = rot / pi * 180
                if texture.image.name.find("ALPHA_BLEND_FILTER") != -1:
                    # smallish hack so textures display correctly in Blender's viewport (which always repeats textures instead of clipping them).
                    # Shouldn't have a negative effect in other engines.
                    slot.scale *= .98
                
                if lyr.UVWSrc >=0 and lyr.UVWSrc <= 10:
                    slot.uv_layer = "UVMap%d" % lyr.UVWSrc
                else:
                    # non vertex-uv source (normal, reflection, camera or depth-based...)
                    # for now we'll simply use reflection as a placeholder
                    # TODO
                    slot.texture_coords = "REFLECTION"
                
                if hasFlags(lyr.state.clampFlags, (pl.hsGMatState.kClampTextureU | pl.hsGMatState.kClampTextureV)):
                    texture.extension = "EXTEND"
                
                if "isStencil" in importProps:
                    slot.use_map_color_diffuse = False
                    slot.use_stencil = True
                else:
                    if hasFlags(lyr.state.blendFlags, pl.hsGMatState.kBlendAdd):
                        slot.blend_type = "ADD"
                        if not firstColorLayerSet:
                            # probably a light glare... disable shadows
                            blMat.use_shadows = False
                            blMat.use_cast_shadows = False
                    if hasFlags(lyr.state.blendFlags, pl.hsGMatState.kBlendSubtract):
                        slot.blend_type = "SUBTRACT"
                    if lyr.state.blendFlags & pl.hsGMatState.kBlendMult or lyr.state.miscFlags & pl.hsGMatState.kMiscLightMap:
                        slot.blend_type = "MULTIPLY"
                    if hasFlags(lyr.state.blendFlags, (pl.hsGMatState.kBlendNoColor | pl.hsGMatState.kBlendNoTexColor)):
                        slot.use_map_color_diffuse = False
                    elif not firstColorLayerSet:
                        blMat.diffuse_color = slot.color
                        blMat.use_mist = not hasFlags(lyr.state.shadeFlags, pl.hsGMatState.kShadeNoFog)
                        if hasFlags(lyr.state.shadeFlags, pl.hsGMatState.kShadeSpecular):
                            blMat.specular_intensity = lyr.specular.alpha
                            blMat.specular_color = Color((lyr.specular.red, lyr.specular.green, lyr.specular.blue))
                            blMat.specular_hardness = lyr.specularPower * 10 # very rough approximation, but should be enough to debug
                        importProps["firstColorLayerSet"] = True
                    
                    if finalColor[0] != 0 or finalColor[1] != 0 or finalColor[0] != 0: # make sure to not make the texture black (this does happen for a few objects...)
                        texture.factor_red = finalColor[0]
                        texture.factor_green = finalColor[1]
                        texture.factor_blue = finalColor[2]
                    
                    texture.use_alpha = hasFlags(lyr.state.blendFlags, pl.hsGMatState.kBlendAlpha) and not ("disableAlpha" in importProps)
                    texture.use_preview_alpha = texture.use_alpha
                    if texture.use_alpha:
                        if not firstColorLayerSet:
                            # first color layer uses alpha, so make sure the material has alpha enabled
                            blMat.use_transparency = True
                            blMat.alpha = opacity
                            slot.use_map_alpha = True
                        else:
                            slot.alpha_factor = opacity
                            if slot.blend_type == "MULTIPLY":
                                blMat.use_transparency = True
                                slot.use_map_alpha = True
                    else:
                        if opacity < 1:
                            if not firstColorLayerSet and not ("disableAlpha" in importProps):
                                blMat.use_transparency = True
                                blMat.alpha = opacity
                            else:
                                slot.diffuse_color_factor = opacity
                    
                    ambient = (lyr.ambient.red, lyr.ambient.green, lyr.ambient.blue, lyr.ambient.alpha)
                    ambientIntensity = (ambient[0] + ambient[1] + ambient[2]) / 3 * ambient[3]
                    if ambientIntensity:
                        slot.use_map_emit = True
                        if not firstColorLayerSet:
                            blMat.emit = ambientIntensity
                        else:
                            slot.emit_factor = opacity
                        if texture.use_alpha:
                            # probably a light glare... disable shadows
                            blMat.use_shadows = False
                            blMat.use_cast_shadows = False
                    
                    if hasFlags(lyr.state.miscFlags, pl.hsGMatState.kMiscBumpLayer):
                        slot.use_map_color_diffuse = False
                        slot.use_map_normal = True
                        slot.normal_factor = opacity
                        texture.use_normal_map = True
            
            elif lyrTexKey.type in (
                    pl.plFactory.kDynamicCamMap,
                    pl.plFactory.kDynamicEnvMap,
                    pl.plFactory.kCubicEnvironmap):
                # not a tangible surface, but actually a reflection
                # so set this material as mirror
                blMat.raytrace_mirror.use = True
                if layerId + 1 < len(plMat.layers) and plMat.layers[layerId + 1].object and plMat.layers[layerId + 1].object.texture and plMat.layers[layerId + 1].object.texture.type == pl.plFactory.kMipmap:
                    # first, create a blTex and use the next layer's alpha as mirror intensity
                    slot = blMat.texture_slots.add()
                    slot.color = Color((finalColor[0], finalColor[1], finalColor[2]))
                    if existingTexture:
                        texture = existingTexture
                    else:
                        texture = bpy.data.textures.new(lyrName, "IMAGE")
                    self.plLayerToBlTex[lyrKey] = texture
                    slot.texture = texture
                    slot.use_rgb_to_intensity = True
                    nextLyrKey = plMat.layers[layerId + 1]
                    nextLyr = nextLyrKey.object
                    image = self.parent.imageImporter.getImageAlpha(nextLyr.texture, True)
                    texture.image = image
                    slot.uv_layer = "UVMap%d" % nextLyr.UVWSrc
                    slot.use_map_color_diffuse = False
                    slot.use_map_raymir = True
                    
                    plMtx = nextLyr.transform.mat
                    mat = Matrix(plMtx)
                    loc, rot, sca = mat.decompose() # TODO handle viewport-computed uvs
                    # Blender scales the texture from its center (user-friendly version),
                    # Plasma from its corner (programmer-friendly version)
                    slot.offset = [
                                    (sca.x - 1) / 2 + loc.x,
                                    (sca.y - 1) / 2 + loc.y,
                                    (sca.z - 1) / 2 + loc.z
                    ]
                    slot.offset.y *= -1 # DX vs OGL
                    slot.scale = sca
                    rot = rot.to_euler().z
                    if rot:
                        texture["rotation"] = rot / pi * 180
                    
                    # import the next layer, but disable alpha sampling
                    importProps["disableAlpha"] = True
                    self.importLayer(plMatKey, blMat, layerId + 1, importProps)
                    importProps.pop("disableAlpha", None)
                    importProps["ignore"] = 1 # ignore importing the next layer
                else:
                    # no next layer, or no next texture ? hmmm...
                    blMat.raytrace_mirror.reflect_factor = .75 # set default reflection intensity (just in case)
                    if lyr.texture.type == pl.plFactory.kCubicEnvironmap:
                        # we have an actual cubemap texture - use its alpha to set mirror intensity...
                        tex = lyr.texture.object
                        if tex.frontFace:
                            ff = tex.frontFace
                            if hasFlags(ff.flags, pl.plBitmap.kAlphaChannelFlag):
                                # texture has alpha - let's fetch a pixel and find out what's its value.
                                blMat.raytrace_mirror.reflect_factor = ff.DecompressImage(0)[3]
                
                if lyrTexKey.type == pl.plFactory.kCubicEnvironmap:
                    # oh, yeah, gotta save the cubemap...
                    self.parent.imageImporter.extractCubemap(lyrTexKey)
            else:
                raise RuntimeError("Unknown texture type for layer %s" % lyrName)
        else:
            # no tex ? just add an empty layer
            slot = blMat.texture_slots.add()
            texture = bpy.data.textures.new(lyrName, "NONE")
            if existingTexture:
                texture = existingTexture
            else:
                self.plLayerToBlTex[lyrKey] = texture
            slot.texture = texture
            slot.use_map_color_diffuse = False
            slot.color = Color((finalColor[0], finalColor[1], finalColor[2]))
            
            if hasFlags(lyr.state.blendFlags, pl.hsGMatState.kBlendAdd):
                slot.blend_type = "ADD"
                if not firstColorLayerSet:
                    # probably a light glare... disable shadows
                    blMat.use_shadows = False
                    blMat.use_cast_shadows = False
            if hasFlags(lyr.state.blendFlags, pl.hsGMatState.kBlendSubtract):
                slot.blend_type = "SUBTRACT"
            if hasFlags(lyr.state.blendFlags, pl.hsGMatState.kBlendMult) \
                    or hasFlags(lyr.state.miscFlags, pl.hsGMatState.kMiscLightMap):
                slot.blend_type = "MULTIPLY"
            if not firstColorLayerSet:
                blMat.diffuse_color = slot.color
                importProps["firstColorLayerSet"] = True
        
        return importProps
    
    def importPiggyBack(self, plMatKey, blMat, layerId, importProps):
        # if "ignore" in importProps:
            # if importProps["ignore"] > 0:
                # importProps["ignore"] -= 1
                # return importProps
        
        plMat = plMatKey.object
        lyrKey = self.getBaseLayerKey(plMat.piggyBacks[layerId])
        lyr = lyrKey.object
        lyrName = stripIllegalChars(lyrKey.name)
        
        if lyr.texture and lyr.texture.object:
            lyrTexKey = lyr.texture
            lyrTex = lyrTexKey.object
            
            if lyrTexKey.type == pl.plFactory.kMipmap:
                # just save the texture
                self.parent.imageImporter.getImage(lyrTexKey)
                self.lightmappedMaterials[blMat] = lyrTex.width
        return importProps
