# -*- coding:utf-8 -*-


"""
This file is part of ZLZ.

ZLZ is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ZLZ is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with ZLZ.  If not, see <https://www.gnu.org/licenses/>
"""


# ZLZ version of Plamza's plClothingItem class, since it doesn't have any binding in HSPlasma


import bpy
from mathutils import *
import PyHSPlasma as pl
from zero_length_zenith.utils import *

class zlClusterGroup:
    
    def __init__(self):
        pass
    
    def read(self, stream):
        pass

