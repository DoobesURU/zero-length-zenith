# -*- coding:utf-8 -*-

"""
This file is part of ZLZ.

ZLZ is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ZLZ is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with ZLZ.  If not, see <https://www.gnu.org/licenses/>
"""


"""
Handles importing individual scene objects.
#"""

import bpy
from mathutils import *
from math import *
import PyHSPlasma as pl
from zero_length_zenith.sceneImporter import SceneImporter
from zero_length_zenith.utils import *
import os

class ObjectImporter:
    
    def __init__(self, parent):
        self.parent = parent
    
    def createObject(self, sceneObjKey):
        """Creates the initial Blender object for a particular scene object."""
        
        # If the object has multiple incompatible modifiers (light + draw, or draw + collision),
        # a master object will be created to which all children will be parented.
        # The key sceneObjKey will reference this master object (or a subobject if there is only one).
        # Interfaces/modifiers that don't require specific Blender data (ex: spawn point) will be attached
        # to this master object.
        dataDict = {} # { plKey: Blender scene object }
        
        if not self.parent.validKey(sceneObjKey):
            return
        
        objName = sceneObjKey.name
        sceneObj = sceneObjKey.object
        print("    %s" % objName)
        
        # these are all the objects we're interested in
        audioKey = sceneObj.audio
        coordKey = sceneObj.coord
        drawKey = sceneObj.draw
        simKey = sceneObj.sim
        interfaces = sceneObj.interfaces
        modifiers = sceneObj.modifiers
        
        # NOW. Create one object per "unique" data (that isn't compatible with any other data).
        # While we're at it, setup as much stuff as we can (meshes, Korman modifiers, etc).
        
        # check audio data
        if self.parent.validKey(audioKey):
            dataDict[audioKey] = self.parent.audioImporter.importAudio(sceneObjKey, audioKey)
        
        # check visuals. Can be of two types: regular drawable meshes, or bones (not rendered). importDraw takes care of both.
        if self.parent.validKey(drawKey):
            blDraw = self.parent.drawImporter.importDraw(sceneObjKey, drawKey)
            dataDict[drawKey] = blDraw
        
        # check collision data
        if self.parent.validKey(simKey):
            if self.parent.validKey(simKey.object.physical):
                dataDict[simKey] = self.parent.physImporter.importPhysical(sceneObjKey, simKey)
        
        # check interfaces
        for interfaceKey in interfaces:
            if self.parent.validKey(interfaceKey):
                # the light types supported in Plasma
                if interfaceKey.type in (
                        pl.plFactory.kOmniLightInfo,
                        pl.plFactory.kDirectionalLightInfo,
                        pl.plFactory.kSpotLightInfo,
                        pl.plFactory.kLimitedDirLightInfo):
                    dataDict[interfaceKey] = self.parent.lightImporter.importLight(sceneObjKey, interfaceKey)
                
                # soft volumes are imported as meshes
                elif interfaceKey.type in (
                        pl.plFactory.kSoftVolumeSimple,
                        # TODO - remove/support extraneous types, when and if needed
                        pl.plFactory.kSoftVolumeComplex,
                        pl.plFactory.kSoftVolumeIntersect,
                        pl.plFactory.kSoftVolumeInvert,
                        pl.plFactory.kSoftVolumeUnion):
                    dataDict[interfaceKey] = self.parent.softVolumeImporter.importSoftVolume(sceneObjKey, interfaceKey)
        
        # check modifiers
        for modifierKey in modifiers:
            if self.parent.validKey(modifierKey):
                if dataDict.get(modifierKey):
                    # already imported - cam modifiers for instance are always listed twice.
                    continue
                if modifierKey.type == pl.plFactory.kCameraModifier:
                    camMod = modifierKey.object
                    if camMod.brain.type == pl.plFactory.kCameraBrain1:
                        # this is a target point, not an actual camera. Hence, we don't give a fluff
                        continue
                    dataDict[modifierKey] = self.parent.camImporter.importCam(sceneObjKey, modifierKey)
                
                elif modifierKey.type == pl.plFactory.kAGModifier:
                    anim = modifierKey.object
                    # register the channel's name for later use
                    self.parent.animImporter.animationChannels[anim.channelName] = sceneObjKey
                
                elif modifierKey.type == pl.plFactory.kAGMasterMod:
                    # we will have to import animation channels later. Right now, we haven't registered all the channel names yet.
                    self.parent.animImporter.masterModsToImport.append((sceneObjKey, modifierKey))
        
        # trim objects that failed to import
        dataDict = { k: v for k, v in dataDict.items() if v }
        
        # OKAY. We got all objects created. Now, let's see what we have...
        
        master = None
        if not dataDict:
            # Not a single object ? Okay, let's just make it an empty...
            master = bpy.data.objects.new(sceneObjKey.name, None)
            self.parent.sceneImporter.appendObjectToScenes(master, SceneImporter.layerEmptiesMisc)
            dataDict[sceneObjKey] = master
        elif len(dataDict) == 1:
            # Only one object. Let's promote it to master !
            for key in tuple(dataDict.keys()): # okay, we still need to iterate over this one object
                master = dataDict[key]
                dataDict[sceneObjKey] = master
                master.name = sceneObjKey.name
        else:
            # Create a master, parent all children to it
            master = bpy.data.objects.new(sceneObjKey.name, None)
            self.parent.sceneImporter.appendObjectToScenes(master, SceneImporter.layerEmptiesMisc)
            for key in dataDict:
                sub = dataDict[key]
                sub.parent = master
                sub.matrix_local = Matrix() # make sure it will stay at the parent's position
            dataDict[sceneObjKey] = master
        
        # position the master object
        if self.parent.validKey(coordKey):
            # has coordinate interface
            # (parenting will be applied later)
            coord = coordKey.object
            glMat = coord.localToParent.glMat
            master.matrix_basis = [glMat[:4], glMat[4:8], glMat[8:12], glMat[12:]]
            
            # register this object as the parent for its children
            # (because we'll need to access object's parents heavily later,
            # and top-down hierarchy is really annoying to work with)
            for childKey in coord.children:
                self.parent.registerParent(childKey, sceneObjKey)
            
            if coordKey.type == pl.plFactory.kFilterCoordInterface:
                master.select = True
                bpy.context.scene.objects.active = master
                bpy.ops.object.plasma_modifier_add(types="animation_filter")
                kormanFilter = master.plasma_modifiers.animation_filter
                
                kormanFilter.no_rotation = hasFlags(coord.filterMask, pl.plFilterCoordInterface.kNoRotation)
                kormanFilter.no_transX = hasFlags(coord.filterMask, pl.plFilterCoordInterface.kNoTransX)
                kormanFilter.no_transY = hasFlags(coord.filterMask, pl.plFilterCoordInterface.kNoTransY)
                kormanFilter.no_transZ = hasFlags(coord.filterMask, pl.plFilterCoordInterface.kNoTransZ)
                master.select = False
        else:
            master.matrix_basis = Matrix() # reset the object's transform, otherwise it seems it picks the last one found...
        
        self.parent.modifierImporter.importModifiers(sceneObjKey, master)
        
        # now, merge dataDict into the main key-to-blObject database
        for key in dataDict:
            self.parent.registerObject(key, dataDict[key])

