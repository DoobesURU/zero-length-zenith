# -*- coding:utf-8 -*-

"""
This file is part of ZLZ.

ZLZ is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ZLZ is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with ZLZ.  If not, see <https://www.gnu.org/licenses/>
"""


"""
Handles importing texture images, or extract their alpha channel.
#"""

import bpy, os
import PyHSPlasma as pl
from zero_length_zenith.utils import *
from mathutils import *

# TODO: Drizzle has a neat fix to force the game to load higher res textures when possible. Might be useful to add it here.

class ImageImporter:
    
    def __init__(self, parent):
        self.parent = parent
        self.knownImages = {} # <plKey (of plMipmap), bpy.type.image>
    
    def extractCubemap(self, plTexKey):
        # Note: this method does not actually return a cubemap for now. Cubemaps are usually only used for
        # reflections, which is better left handled by Blender itself.
        # However, we do extract the cubemap in case someone wants to have fun with it...
        # (We don't bother exporting it as a multivolume DDS - I'm not sure plDDSurface supports it...
        # Artifacts in cubemaps are no big deal anyway.)
        
        if plTexKey in self.knownImages:
            return
        
        plTex = plTexKey.object
        
        texes = []
        for tex, faceName in ((plTex.backFace, "back"),
                              (plTex.bottomFace, "bottom"),
                              (plTex.frontFace, "front"),
                              (plTex.leftFace, "left"),
                              (plTex.rightFace, "right"),
                              (plTex.topFace, "top")):
            texName = stripIllegalChars(plTexKey.name) + faceName
            # this is one of the few cases where we can rely on Blender's library and the object's name
            image = bpy.data.images.get(texName)
            if not image:
                # Let's simply use the same path as PyPRP: the TMP_Textures folder in the import folder...
                cachedFilePath = os.path.join(self.parent.texFolderLocation, texName + ".png")
                
                if self.parent.config["reuseTextures"] and os.path.exists(cachedFilePath):
                    # image was previously extracted, simply reload it - should be faster.
                    image = bpy.data.images.load(cachedFilePath, check_existing=True)
                else:
                    # image does not yet exist, create it
                    pixels = tex.DecompressImage(0)
                    image = bpy.data.images.new(texName, tex.width, tex.height, alpha=hasFlags(plTex.flags, pl.plMipmap.kAlphaChannelFlag))
                    image.file_format = 'PNG'
                    
                    # pixels is in direct3d order, which means the image is upside down.
                    # move pixels around to put it in the correct order...
                    lines = [
                        pixels[i * tex.width * 4 : (i + 1) * tex.width * 4]
                        for i in range(tex.height - 1, -1, -1) # flip the lines
                    ]
                    newPixels = [
                        pixel / 255
                        for line in lines # python is dope
                        for pixel in line
                    ]
                    image.pixels = newPixels
                    
                    # Blender is keen on deleting images even if they are dirty, so make sure to save it somewhere safe
                    image.filepath_raw = cachedFilePath
                    image.save()
                
                texes.append(image)
        
        self.knownImages[plTexKey] = texes
    
    def getImage(self, plTexKey):
        plTex = plTexKey.object
        texName = stripIllegalChars(plTexKey.name)
        image = self.knownImages.get(plTexKey)
        if image:
            return image
        
        extension = ".png" # use PNG by default
        
        if self.parent.config["textureFormat"] == "ORIGINAL":
            # we are importing the textures without converting them to another format (which is usually better)
            
            if plTex.compressionType in (pl.plMipmap.kUncompressed, pl.plMipmap.kDirectXCompression):
                # either block-compression, or raw pixel data. Both work perfectly with DDS.
                extension = ".dds"
            
            else:
                print("WARNING - texture %s has compression type %s, which cannot be extracted without reconverting it to PNG." % (plTexKey.name, plTex.compressionType))
            
            """
            elif plTex.compressionType == pl.plMipmap.kJPEGCompression:
                # Okay, so looking as HSPlasma's source, it seems the JPEG is still embedded in a DDS structure (it's just not block-compressed).
                # This also means the texture may still contain an alpha channel (unlike regular JPEGs).
                # Therefore I *think* we can extract it as DDS without risking recompression.
                extension = ".dds"
                
                # BUUUUT, there is an issue with color channels being swapped in this DDS.
                # https://github.com/H-uru/libhsplasma/pull/127
                # Mhmmkay, until this is fixed, and we're sure we can extract as DDS without risking recompression, let's just crash.
                raise RuntimeError("JPEG-compressed images cannot be extracted yet without risking recompression.\nPlease go pester the GoW for a fix, and use PNG extraction in the meantime.")
            
            # elif pl.plMipmap.kPNGCompression: # not sure if this is ever used ?
            
            else:
                raise RuntimeError("Unsupported texture format %s for texture %s !" % (plTex.compressionType, plTexKey.name))
            
            #"""
        
        # File path and name for the extracted texture.
        # Let's simply use the same path as PyPRP: the TMP_Textures folder in the import folder...
        cachedFilePath = os.path.join(self.parent.texFolderLocation, texName + extension)
        
        if self.parent.config["reuseTextures"] and os.path.exists(cachedFilePath):
            # image was previously extracted, simply reload it - should be faster.
            image = bpy.data.images.load(cachedFilePath, check_existing=True)
        else:
            # image does not yet exist, create it
            if extension == ".png":
                # convert texture to PNG. Easier to reuse, but not recommended due to potential lossy recompression.
                pixels = plTex.DecompressImage(0)
                image = bpy.data.images.new(texName, plTex.width, plTex.height, alpha=hasFlags(plTex.flags, pl.plMipmap.kAlphaChannelFlag))
                image.file_format = 'PNG'
                
                # pixels are in direct3d order, which means the image is upside down.
                # move pixels around to put the lines in the correct order... This is a bit slow, but not horribly so.
                lines = [
                    pixels[i * plTex.width * 4 : (i + 1) * plTex.width * 4]
                    for i in range(plTex.height - 1, -1, -1) # flip the lines
                ]
                newPixels = [
                    pixel / 255
                    for line in lines # python is dope
                    for pixel in line
                ]
                image.pixels = newPixels
                
                # Blender is keen on deleting images even if they are dirty, so make sure to save it somewhere safe
                # A workaround is to save the image to a file, pack it, then delete it. But saving the image directly to the disk
                # is generally the behavior we want anyway...
                image.filepath_raw = cachedFilePath
                image.save()
            
            elif extension == ".dds":
                # write to DDS
                surf = pl.plDDSurface()
                surf.setFromMipmap(plTex)
                stream = pl.hsFileStream()
                stream.open(cachedFilePath, pl.fmCreate)
                surf.write(stream)
                stream.close()
                # and reload it in Blender's memory.
                image = bpy.data.images.load(cachedFilePath, check_existing=True)
            
            else:
                raise RuntimeError("Unsupported texture format %s for texture %s !" % (plTex.compressionType, plTexKey.name))
            
        self.knownImages[plTexKey] = image
        
        return image
    
    def getImageAlpha(self, plTexKey, invert):
        # NOTE: extracting the alpha channel should only be used to provide metallic reflection layers in Blender.
        # (When importing into other engines, it's best to use a custom shader relying on the original texture's alpha channel
        # to avoid compression artefacts.)
        image = self.knownImages.get((plTexKey, "alpha"))
        if image:
            return image
        
        plTex = plTexKey.object
        texName = stripIllegalChars(plTexKey.name) + "_alpha"
        
        # Let's simply use the same path as PyPRP: the TMP_Textures folder in the import folder...
        cachedFilePath = os.path.join(self.parent.texFolderLocation, texName + ".png")
        
        if self.parent.config["reuseTextures"] and os.path.exists(cachedFilePath):
            # image was previously extracted, simply reload it - should be faster.
            image = bpy.data.images.load(cachedFilePath, check_existing=True)
        else:
            # image does not yet exist, create it
            pixels = plTex.DecompressImage(0)
            image = bpy.data.images.new(texName, plTex.width, plTex.height, alpha=False)
            image.file_format = 'PNG'
            
            # pixels is in direct3d order, which means the image is upside down.
            # move pixels around to put it in the correct order...
            lines = [
                pixels[i * plTex.width * 4 : (i + 1) * plTex.width * 4]
                for i in range(plTex.height - 1, -1, -1) # flip the lines
            ]
            newPixels = [
                pixel / 255
                for line in lines # python is dope
                for pixel in line
            ]
            for i in range(len(newPixels) // 4):
                alpha = newPixels[i * 4 + 3]
                if invert:
                    alpha = 1 - alpha
                newPixels[i * 4] = alpha
                newPixels[i * 4 + 1] = alpha
                newPixels[i * 4 + 2] = alpha
                newPixels[i * 4 + 3] = 1
            image.pixels = newPixels
            
            # Blender is keen on deleting images even if they are dirty, so make sure to save it somewhere safe
            # Let's simply use the same path as PyPRP: the TMP_Textures folder in the import folder...
            image.filepath_raw = cachedFilePath
            image.save()
        
        self.knownImages[(plTexKey, "alpha")] = image
        
        return image
    
    def getDynamicEnvMap(self, plEnvMapKey): # not exactly an image, but works as texture.
        # TODO - support DynamicCamMap.
        if not plEnvMapKey or not plEnvMapKey.object:
            return None
        
        existing = self.knownImages.get(plEnvMapKey)
        if existing:
            return existing
        
        plEnv = plEnvMapKey.object
        texName = stripIllegalChars(plEnvMapKey.name)
        texture = bpy.data.textures.new(texName, type="ENVIRONMENT_MAP")
        envmap = texture.environment_map
        
        texture.plasma_layer.envmap_color = Color((plEnv.color.red, plEnv.color.green, plEnv.color.blue))
        texture.plasma_layer.opacity = plEnv.color.alpha * 100
        
        viewpoint = bpy.data.objects.new(plEnvMapKey.name + "_VIEWPOINT", None)
        viewpoint.location = Vector((plEnv.position.X, plEnv.position.Y, plEnv.position.Z))
        envmap.viewpoint_object = viewpoint
        envmap.resolution = plEnv.width
        envmap.clip_start = plEnv.hither
        envmap.clip_end = plEnv.yon
        envmap.source = "ANIMATED" if plEnv.refreshRate else "STATIC"
        if len(plEnv.visRegions):
            print("WARNING - VisRegions not set for envmap %s" % plEnvMapKey.name)
        # texture.plasma_layer.vis_regions = [???] # TODO - list of objects with a VisRegion modifier
        
        return (texture, viewpoint)

