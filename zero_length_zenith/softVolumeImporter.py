# -*- coding:utf-8 -*-

"""
This file is part of ZLZ.

ZLZ is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ZLZ is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with ZLZ.  If not, see <https://www.gnu.org/licenses/>
"""


"""
Handles setting up softvolumes and rebuilding their mesh.
#"""

import bpy, bmesh
import PyHSPlasma as pl
from mathutils import *
from math import *
from zero_length_zenith.sceneImporter import SceneImporter
from zero_length_zenith.utils import *

class SoftVolumeImporter:
    
    def __init__(self, parent):
        self.parent = parent
        self.camsToFinishSetup = []
    
    def importSoftVolume(self, sceneObjKey, volumeKey):
        if not self.parent.validKey(volumeKey):
            return None
        
        if volumeKey.type != pl.plFactory.kSoftVolumeSimple:
            raise NotImplementedError("Unexpected or unsupported soft volume type %d" % volumeKey.type) # yeah, could be a TypeError instead but HEY this is WIP so stop bothering me.
        
        plVolumeMod = volumeKey.object
        
        mesh = bpy.data.meshes.new(volumeKey.name)
        blObj = bpy.data.objects.new(sceneObjKey.name + "_SOFTVOLUME", mesh)
        self.parent.sceneImporter.appendObjectToScenes(blObj, SceneImporter.layerSoftVolumes)
        blObj.draw_type = "WIRE"
        blObj.hide_render = True
        
        # rebuild the mesh.
        # Ideally we would compute the vertices at the intersection of each of the SV's planes.
        # But I don't really know how to do that.
        # So instead let's use a correct but ugly workaround: place one quad where each plane is supposed to be.
        # This will be correct enough that Korman accepts reexporting it, and this way I don't have to spend all night on it.
        
        # first, compute the bounding volume of the SV.
        # This has no real use, it's just so we can scale our floating faces a bit better.
        lowerBound = [99999, 99999, 99999]
        upperBound = [-99999, -99999, -99999]
        for plane in plVolumeMod.volume.planes:
            if lowerBound[0] > plane.pos.X: lowerBound[0] = plane.pos.X
            if lowerBound[1] > plane.pos.Y: lowerBound[1] = plane.pos.Y
            if lowerBound[2] > plane.pos.Z: lowerBound[2] = plane.pos.Z
            if upperBound[0] < plane.pos.X: upperBound[0] = plane.pos.X
            if upperBound[1] < plane.pos.Y: upperBound[1] = plane.pos.Y
            if upperBound[2] < plane.pos.Z: upperBound[2] = plane.pos.Z
        diagonal = (upperBound[0] - lowerBound[0],
                    upperBound[1] - lowerBound[1],
                    upperBound[2] - lowerBound[2])
        bboxCenter = Vector(((upperBound[0] + lowerBound[0]) / 2,
                             (upperBound[1] + lowerBound[1]) / 2,
                             (upperBound[2] + lowerBound[2]) / 2))
        volumeApproxSize = sqrt(diagonal[0] ** 2 + diagonal[1] ** 2 + diagonal[2] ** 2)
        # the more planes we have in our SV, the smaller each of our floating face get.
        # The following formula is random but should yield fairly acceptable results.
        faceSizeMultiplier = volumeApproxSize / (len(plVolumeMod.volume.planes) * 2)
        
        bm = bmesh.new()
        assert(isinstance(plVolumeMod.volume, pl.plConvexIsect))
        
        # create 4 vertices for each plane
        for plane in plVolumeMod.volume.planes:
            pos = Vector((plane.pos.X, plane.pos.Y, plane.pos.Z))
            norm = Vector((plane.norm.X, plane.norm.Y, plane.norm.Z))
            sideA = norm.cross(Vector((0, 0, 1))).normalized()
            if sideA.length < .01:
                sideA = Vector((1, 0, 0))
            sideB = norm.cross(sideA).normalized()
            # now create a quad
            # oh, I forgot to mention something. plane.pos is incorrect. It's on the correct plane, sure, but it uses
            # the position of one of the original vertices instead of the plane's center. So we have to compute that too.
            # Sigh... Just align the normal with the bounding box center...
            newPos = bboxCenter + (pos - bboxCenter).project(norm)
            pos0 = newPos + sideA * faceSizeMultiplier
            pos1 = newPos + sideB * faceSizeMultiplier
            pos2 = newPos - sideA * faceSizeMultiplier
            pos3 = newPos - sideB * faceSizeMultiplier
            bm.verts.new((pos0.x, pos0.y, pos0.z))
            bm.verts.new((pos1.x, pos1.y, pos1.z))
            bm.verts.new((pos2.x, pos2.y, pos2.z))
            bm.verts.new((pos3.x, pos3.y, pos3.z))
        
        bm.verts.ensure_lookup_table()
        
        # now make faces for each vert quatuor
        for i in range(len(plVolumeMod.volume.planes)):
            bm.faces.new(bm.verts[i * 4 : i * 4 + 4])
        
        bm.to_mesh(mesh)
        bm.free()
        del bm
        
        blObj.select = True
        bpy.context.scene.objects.active = blObj
        bpy.ops.object.plasma_modifier_add(types="softvolume")
        kormanMod = blObj.plasma_modifiers.softvolume
        
        kormanMod.use_nodes = False
        kormanMod.invert = False
        kormanMod.inside_strength = plVolumeMod.insideStrength * 100
        kormanMod.outside_strength = plVolumeMod.outsideStrength * 100
        kormanMod.soft_distance = plVolumeMod.softDist
        
        blObj.select = False
        
        return blObj

