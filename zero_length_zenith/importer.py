# -*- coding:utf-8 -*-

"""
This file is part of ZLZ.

ZLZ is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ZLZ is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with ZLZ.  If not, see <https://www.gnu.org/licenses/>
"""


"""
Operator used for importing Ages, duh.
#"""

import bpy, os, time, sys, re, ctypes, traceback
from bpy.props import *
from zero_length_zenith.log import Log
from zero_length_zenith.sceneImporter import SceneImporter
from zero_length_zenith.objectImporter import ObjectImporter
from zero_length_zenith.animImporter import AnimImporter
from zero_length_zenith.audioImporter import AudioImporter
from zero_length_zenith.drawImporter import DrawImporter
from zero_length_zenith.physImporter import PhysImporter
from zero_length_zenith.lightImporter import LightImporter
from zero_length_zenith.camImporter import CamImporter
from zero_length_zenith.softVolumeImporter import SoftVolumeImporter
from zero_length_zenith.modifierImporter import ModifierImporter
from zero_length_zenith.matImporter import MatImporter
from zero_length_zenith.imageImporter import ImageImporter
import PyHSPlasma as pl
from mathutils import *


#-#-#-# Registration #-#-#-#

def menu_func_import(self, context):
    self.layout.operator(Importer.bl_idname, text="ZLZ Uru importer")


def register():
    bpy.utils.register_class(Importer)
    bpy.types.INFO_MT_file_import.append(menu_func_import)

def unregister():
    bpy.utils.unregister_class(Importer)
    bpy.types.INFO_MT_file_import.remove(menu_func_import)


#-#-#-# Import Class #-#-#-#

class Importer(bpy.types.Operator):
    """Uru importer"""
    bl_idname = "import.zlzimport"
    bl_label = "Import .age or .prp"
    
    filepath = StringProperty(subtype='FILE_PATH')
    
    clearScene = BoolProperty(name="Clear scene", default=True,
                        description="Delete any object in the scene before import")
    
    reuseTextures = BoolProperty(name="Reuse textures", default=True,
                        description="This will skip extracting textures which are already in the TMP_Textures folder and load those instead")
    
    animAttachType = EnumProperty(name="Anim attachment",
                        items=(('OBJECT', "Original object", "Attach to the animated object itself. May position the object incorrectly"),
                               ('HOLDER', "Holder object", "Attach to a dummy animation holder empty. Avoids incorrectly placing the original object")),
                        default="OBJECT",
                        description="Which object to attach animations to")
    
    textureFormat = EnumProperty(name="Texture format",
                        items=(('PNG', "PNG", "Convert to PNG. More widely supported than DDS, but reimporting in 3D engines will degrade quality due to the successive lossy compressions"),
                               ('ORIGINAL', "Original (DDS)", "Extract as DDS (or the original file format when possible). Not all softwares support DDS, but this means you can reuse textures in other engines without worrying about degrading texture quality")),
                        default="ORIGINAL",
                        description="Format to extract textures in")
    
    normalImportType = EnumProperty(name="Normals",
                        items=(('RECOMPUTE', "Recompute", "Remove double vertices, then recompute sharp edge. Usually the best option"),
                               ('NONE', "None", "Don't import normals, and don't remove doubles")),
                        default="RECOMPUTE",
                        description="How to import normals")
    
    frameRate = EnumProperty(name="Framerate",
                        items=(('60', "60", "Smoother playback in Blender, recommended for most monitors"),
                               ('30', "30", "Choppier playback in Blender, matches how animations are stored in PRPs. There shouldn't be any need to go so low unless you're having specific issues with 60"),
                               ('current', "Use the current framerate", "Don't change Blender's playback framerate. Animations speed will try to fit this framerate. Note that this may result in odd placement of keyframes due to rounding errors")),
                        default="60",
                        description="For some reason, Blender doesn't offer a simple way to change playback framerate without affecting playback speed too. Setting your preference framerate here changes Blender's playback speed and ensures imported animations will play at the correct speed (as long as you don't change the framerate; if you do you will have to mess with Blender's annoying time-remapping feature).\nThis will have no negative effect when re-exporting with Korman")
                        # (fun fact: Blender's abusive hand-holding automatically appends an extra dot at the end of enum descriptions, even if there is already one.
                        # ...But NOT in every type of hover tooltip. *%$!)
    
    profile = BoolProperty(name="Profile", default=False,
                        description="Print profiling information at the end of importing (only useful for debugging)")
    
    def __init__(self):
        # where the PRP or AGE file is located
        self.dataFolderLocation = None
        
        # where to store extracted textures (TMP_Textures folder)
        self.texFolderLocation = None
        
        # where any sound FX should be located
        self.sfxFolderLocation = None
        
        # a simpler way to access the above Blender Properties
        self.config = None
        
        # various importers used
        # Those will all have this object as parent for simplicity.
        self.sceneImporter = None
        self.objectImporter = None
        self.animImporter = None
        self.audioImporter = None
        self.drawImporter = None
        self.physImporter = None
        self.lightImporter = None
        self.camImporter = None
        self.softVolumeImporter = None
        self.modifierImporter = None
        self.matImporter = None
        self.imageImporter = None
        
        # a HSPlasma resource manager for loading stuff
        self.rmgr = None
        
        # maps <plKey: Blender object>
        self.plKeysToBlObj = {}
        
        # stores the parent for each object key
        self.objectParents = {}
        
        # stores the last frame for the longest anim in the scene
        self.endFrame = 0
        
        self.numMissingReferences = 0
    
    def execute(self, context):
        """Executes import, called after selecting the file in Blender"""
        
        if self.profile:
            import cProfile
            pr = cProfile.Profile()
            pr.enable()
        
        self.performImport()
        
        if self.profile:
            pr.disable()
            pr.print_stats(sort='time')
            # it seems most of the import time is spent on scene update()s, which are automatically triggered
            # by ALL operators (see ops.py: BPyOpsSubModOp._scene_update). Obviously this is fairly bad when
            # calling operators from scripts, especially since it grows longer the bigger our scenes get (see Gahreesen and
            # its fuckton of separate wall blocker objects...).
            # Not much we can do about it though, Blender obviously didn't include an off switch.
            # Seems Blender 2.8 changed the way it handles object graph, which means scene updates shouldn't suck as much.
            # But for now, we'll have to live with it. This means we won't squeeze much more performance.
        
        return {"FINISHED"}
    
    def performImport(self):
        # first, force the console visible on Windoz (simplified version of code stolen from Korman)
        if sys.platform == "win32":
            hwnd = ctypes.windll.kernel32.GetConsoleWindow()
            if not bool(ctypes.windll.user32.IsWindowVisible(hwnd)):
                bpy.ops.wm.console_toggle()
                ctypes.windll.user32.ShowWindow(hwnd, 1)
                ctypes.windll.user32.BringWindowToTop(hwnd)
        
        # force Korman active, hence making it a hard requirement. We'll use some of its operators to handle modifiers and such.
        try:
            bpy.context.scene.render.engine = "PLASMA_GAME"
        except TypeError:
            raise TypeError("Korman needs to be installed and active before importing can take place.")
        
        filePath = os.path.abspath(self.filepath)
        if not filePath.lower().endswith(".age") and not filePath.lower().endswith(".prp"):
            raise RuntimeError("Importer only supports .age and .prp file, aborting.")
        
        start = time.clock()
        log = Log(sys.stdout, filePath + ".log", "w")
        sys.stdout = log
        
        self.config = {
            'reuseTextures': self.reuseTextures,
            'attachAnimationsToObjects': self.animAttachType == "OBJECT",
            'textureFormat': self.textureFormat,
            'normalImportType': self.normalImportType,
        }
        
        self.dataFolderLocation = os.path.dirname(filePath)
        self.texFolderLocation = os.path.join(self.dataFolderLocation, "TMP_Textures")
        self.sfxFolderLocation = os.path.join(os.path.dirname(self.dataFolderLocation), "sfx")
        
        if not os.path.exists(self.texFolderLocation):
            os.mkdir(self.texFolderLocation)
        
        self.rmgr = pl.plResManager()
        self.numMissingReferences = 0
        
        self.sceneImporter = SceneImporter(self)
        self.objectImporter = ObjectImporter(self)
        self.animImporter = AnimImporter(self)
        self.audioImporter = AudioImporter(self)
        self.drawImporter = DrawImporter(self)
        self.physImporter = PhysImporter(self)
        self.lightImporter = LightImporter(self)
        self.camImporter = CamImporter(self)
        self.softVolumeImporter = SoftVolumeImporter(self)
        self.modifierImporter = ModifierImporter(self)
        self.matImporter = MatImporter(self)
        self.imageImporter = ImageImporter(self)
        
        if self.clearScene:
            # kill your good friend the default cube. You monster.
            curObjects = list(bpy.context.scene.objects)
            for obj in curObjects:
                bpy.context.scene.objects.unlink(obj)
        
        if self.frameRate == "60":
            print("Changing Blender's framerate to 60.")
            bpy.context.scene.render.fps_base = 1
            bpy.context.scene.render.fps = 60
        elif self.frameRate == "30":
            print("Changing Blender's framerate to 30.")
            bpy.context.scene.render.fps_base = 1
            bpy.context.scene.render.fps = 30
        else:
            fr = bpy.context.scene.render.fps
            base = bpy.context.scene.render.fps_base
            actualFramerate = fr / base
            print("Not changing Blender's framerate of %.2f." % actualFramerate)
            if actualFramerate != int(actualFramerate) or actualFramerate < 30:
                print("(Which is really not a good framerate, if you want my opinion.)")
            elif actualFramerate % 30:
                print("(Not a multiple of 30, but okay-ish.)")
            else:
                print("(Multiple of 30, should be fine.)")
        
        # create the Blender objects corresponding to each Plasma sceneobject.
        # This also creates meshes, materials, Korman modifiers n'stuff.
        # This does NOT setup any reference between objects themselves yet (such as parenting).
        if filePath.lower().endswith(".age"):
            self.importAge(filePath)
        elif filePath.lower().endswith(".prp"):
            self.importPrp(filePath)
        
        # don't remember why, but a scene update is a good idea right now.
        bpy.context.scene.update()
        
        # got everything imported. Now we can setup references between objects without worrying
        # about cross-PRP references.
        
        print("Setting up armature hierarchy...")
        self.drawImporter.rebuildArmatures()
        
        print("Setting up hierarchy...")
        self.sceneImporter.rebuildHierarchy()
        
        print("Creating animations...")
        self.animImporter.importAnimations()
        
        print("Setting up references...")
        self.camImporter.setupReferences()
        self.modifierImporter.setupReferences()
        
        print("Repositioning world-space collisions...")
        self.physImporter.repositionWorldSpacePhysicals()
        
        for scene in bpy.data.scenes:
            scene.frame_end = self.endFrame
        
        print("Done in %.2f seconds." % (time.clock()-start))
        
        if self.numMissingReferences:
            print("WARNING - failed to find references %d times. This means some objects failed to import properly. Check the logs for details..." % self.numMissingReferences)
        
        sys.stdout = sys.__stdout__
        log.close()
    
    def importAge(self, filePath):
        print("\n\n--------------------------------------\n---> Importing %s" % filePath)
        
        age = self.rmgr.ReadAge(filePath, True)
        print("Age name is %s" % age.name)
        
        # get the age's plasma version (not sure what's the best practice here, will just fetch it from a page location...)
        ageVersion = pl.pvPots
        for location in self.rmgr.getLocations():
            if location.prefix != age.seqPrefix:
                continue
            ageVersion = location.version
            break
        
        # import world settings. Should make it easier to reexport with Korman.
        world = bpy.data.worlds.new(age.name)
        bpy.context.scene.world = world
        
        # import age config
        ageSettings = world.plasma_age
        ageSettings.day_length = age.dayLength
        ageSettings.start_time = age.startDateTime
        ageSettings.seq_prefix = age.seqPrefix
        ageSettings.age_name = age.name
        ageSettings.age_sdl = False
        for i in range(age.getNumCommonPages(ageVersion)):
            if age.getCommonPage(i, ageVersion)[0] == "BuiltIn":
                ageSettings.age_sdl = True
        
        # (page settings will be setup by the sceneImporter)
        
        # import fni stuff
        fniLocation = os.path.join(self.dataFolderLocation, age.name + ".fni")
        if os.path.exists(fniLocation):
            print("Importing FNI")
            self.importFni(fniLocation, world)
        
        print("Importing pages...")
        # iterate through the pages to import stuff...
        for location in self.rmgr.getLocations():
            if location.prefix != age.seqPrefix:
                # we didn't load any other Age, how can this even happen ? whatever, just ignore it
                continue
            # this page comes from the Age we loaded (no shit sherlock)
            self.sceneImporter.importScene(location)
            # set the page's "auto load" flag. This comes from the Age itself, so the sceneImporter doesn't know anything about it.
            # (unfortunately, accessing this property is a bit tricky...)
            for page in ageSettings.pages:
                # first, we must find the Korman page matching the page we just imported...
                if page.seq_suffix == location.page:
                    for pageIndex in range(age.getNumPages()):
                        # then, find the page entry in the .age file corresponding to the page... (yeah this is getting a bit confusing)
                        tuple = age.getPage(pageIndex)
                        if tuple[1] == page.seq_suffix:
                            page.auto_load = tuple[2] == 0
        
        print("\n\nDone !")
    
    def importPrp(self, filePath):
        print("\n\nWARNING - importing single PRPs is very likely to break some references. Never use it if you intend to re-export the Age.")
        print("--------------------------------------\n---> Importing %s" % filePath)
        
        # now this is going to be a bit tricky...
        # to not break cross-prp references, we need to load the full Age, but import only one PRP.
        # Which means we need to find the .age path. If it does not exist, then try to load the single PRP
        
        prpFileName = os.path.basename(filePath)
        district = "_District_"
        districtIndex = prpFileName.find(district)
        districtLen = len(district)
        if districtIndex != -1:
            # simple case where the file contains the district string... this is a safe way to guess the Age name
            ageFileName = os.path.dirname(filePath) + os.sep + prpFileName[:districtIndex] + ".age"
            pageName = prpFileName[districtIndex + districtLen : -len(".prp")]
        else:
            # ugh, we have to guess the agename based on the underscores only... hopefully the Age name itself doesn't contain any underscore...
            underscoreIndex = prpFileName.find('_')
            ageFileName = os.path.dirname(filePath) + os.sep + prpFileName[:underscoreIndex] + ".age"
            pageName = prpFileName[underscoreIndex + 1 : -len(".prp")]
        
        if os.path.exists(ageFileName):
            print("Import as PRP: .age file present, this should somewhat reduce the number of broken cross references...")
            age = self.rmgr.ReadAge(ageFileName, True)
            print("Age name is %s" % age.name)
            pageLoc = None
            for i in range(age.getNumPages()):
                pageInfoTuple = age.getPage(i)
                if pageInfoTuple[0] == pageName:
                    pageLoc = pageInfoTuple[1]
            
            if pageLoc == None:
                raise RuntimeError("Couldn't find PRP ID. Make sure it is registered in the .age file...")
            
            # now that things are loaded, iterate through the pages to import stuff...
            for location in self.rmgr.getLocations():
                if location.prefix != age.seqPrefix:
                    # we didn't load any other Age, how can this even happen ? whatever, just ignore it
                    continue
                if location.page != pageLoc:
                    # not the page we want... Skip it.
                    continue
                # this page comes from the Age we loaded (no shit sherlock)
                self.sceneImporter.importScene(location)
        else:
            print("WARNING: couldn't find file %s for this PRP. This will likely result in broken references and missing textures..." % ageFileName)
            page = self.rmgr.ReadPage(filePath)
            print("Age name is %s, page name is %s" % (page.age, page.page))
            
            # now that things are loaded, iterate through the pages to import stuff...
            for location in self.rmgr.getLocations():
                if location.prefix != page.location.prefix:
                    # we didn't load any other Age, how can this even happen ? whatever, just ignore it
                    continue
                # this page comes from the Age we loaded (no shit sherlock)
                self.sceneImporter.importScene(location)
        
        print("\n\nDone !")
    
    def importFni(self, fniLocation, world):
        fni = world.plasma_fni
        fniStream = pl.plEncryptedStream()
        fniStream.open(fniLocation, pl.fmRead, pl.plEncryptedStream.kEncAuto)
        hasFog = False
        while not fniStream.eof():
            line = fniStream.readLine()
            commentLocation = line.find("#")
            if commentLocation != -1:
                line = line[:line.find("#")]
            line = line.strip()
            lowerline = line.lower()
            if not line:
                # empty line or comment, ignore
                continue
            
            if lowerline.startswith("Graphics.Renderer.SetClearColor".lower()):
                splittedLine = re.sub(" +", " ", lowerline).split(" ")
                r = min(max(float(splittedLine[1]), 0), 1)
                g = min(max(float(splittedLine[2]), 0), 1)
                b = min(max(float(splittedLine[3]), 0), 1)
                fni.clear_color = Color((r, g, b))
            
            elif lowerline.startswith("Graphics.Renderer.Fog.SetDefColor".lower()):
                splittedLine = re.sub(" +", " ", lowerline).split(" ")
                r = min(max(float(splittedLine[1]), 0), 1)
                g = min(max(float(splittedLine[2]), 0), 1)
                b = min(max(float(splittedLine[3]), 0), 1)
                world.horizon_color = fni.fog_color = Color((r, g, b))
            
            elif lowerline.startswith("Graphics.Renderer.Fog.SetDefLinear".lower()):
                splittedLine = re.sub(" +", " ", lowerline).split(" ")
                start = float(splittedLine[1])
                end = float(splittedLine[2])
                density = float(splittedLine[3])
                if density * end != 0:
                    hasFog = True
                    world.mist_settings.use_mist = True
                    world.mist_settings.falloff = "LINEAR"
                    world.mist_settings.start = start
                    world.mist_settings.depth = end - start
                    fni.fog_method = "linear"
                    fni.fog_start = start
                    fni.fog_end = end
                    fni.fog_density = density
            
            elif lowerline.startswith("Graphics.Renderer.Fog.SetDefExp2".lower()):
                splittedLine = re.sub(" +", " ", lowerline).split(" ")
                end = float(splittedLine[1])
                density = float(splittedLine[2])
                if density * end != 0:
                    hasFog = True
                    world.mist_settings.use_mist = True
                    world.mist_settings.falloff = "QUADRATIC"
                    world.mist_settings.start = 0
                    world.mist_settings.depth = end
                    fni.fog_method = "exp2"
                    fni.fog_end = end
                    fni.fog_density = density
            
            elif lowerline.startswith("Graphics.Renderer.SetYon".lower()):
                splittedLine = re.sub(" +", " ", lowerline).split(" ")
                yon = int(splittedLine[1])
                fni.yon = yon
                for area in bpy.context.window.screen.areas:
                    if area.type != "VIEW_3D":
                        continue
                    for space in area.spaces:
                        if space.type != "VIEW_3D":
                            continue
                        space.clip_end = yon
                        
                        # Eh, also set hither while we're at it. Plasma's default should be around 0.1 foot ?
                        # Personally, I prefer at least 0.328 feet (10 cm), since that causes less issues
                        # with Blender's ambient occlusion and stuff. I'm usually zoomed pretty far out anyway.
                        space.clip_start = .328
            
            elif lowerline.startswith("Graphics.Renderer.Gamma2".lower()):
                # sometime present but bad practice. Recognize it, don't process it.
                pass
            
            else:
                print("WARNING - unparsed FNI line:\n%s" % line)
        
        if not hasFog:
            fni.fog_density = fni.fog_start = fni.fog_end = 0
            fni.fog_method = "none"
            # huh, seems Korman re-inits this to its default. A bit annoying, but no big deal...
    
    def validKey(self, plKey):
        """Returns true if the given plKey is valid, and if the object it references is loaded.
        If the key is null, simply return false.
        If the key is valid but the referenced object is not loaded, prints a debug message and returns false."""
        if plKey:
            if plKey.object:
                return True
            else:
                traceback.print_stack()
                print("WARNING - plKey %s (type %d) cross-references an unloaded PRP. Skipping." % (plKey.name, plKey.type))
                self.numMissingReferences += 1
                return False
        else:
            return False
    
    def registerObject(self, plKey, blObject):
        """Registers the given plKey as having a Blender object equivalent"""
        self.plKeysToBlObj[plKey] = blObject
    
    def getBlObjectFromKey(self, plKey):
        """Returns the Blender object corresponding to a plKey (or none if not imported)."""
        if not plKey:
            return None # normal null key
        
        result = self.plKeysToBlObj.get(plKey)
        if result:
            return result
        
        traceback.print_stack()
        print("WARNING - trying to get a Blender object from plKey %s, but it failed to import correctly or was not imported yet. Skipping." % plKey)
        return None
    
    def registerParent(self, childKey, parentKey):
        """Registers the scene object with the given plKey as being a child of the other given object's key"""
        self.objectParents[childKey] = parentKey
    
    def getParent(self, childKey):
        """Returns the key of the parent object registered with registerParent, or None if the object doesn't have a parent."""
        return self.objectParents.get(childKey)
    
    def invoke(self, context, event):
        """Displays filepicker interface, after selecting import menu"""
        print("\nZLZ Uru Importer: Be Invoked !")
        WindowManager = context.window_manager
        WindowManager.fileselect_add(self)
        return {'RUNNING_MODAL'}

