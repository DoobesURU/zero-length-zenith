# -*- coding:utf-8 -*-

"""
This file is part of ZLZ.

ZLZ is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ZLZ is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with ZLZ.  If not, see <https://www.gnu.org/licenses/>
"""


"""
Handles registration of the plugin.
#"""

bl_info = {
    'name': 'ZeroLengthZenith',
    'author': 'Sirius',
    'version': (0, 0, 1),
    'blender': (2, 7, 7),
    'location': 'File > Import > ZeroLengthZenith Uru Age Importer',
    'description': 'ZLZ',
    'category': 'Import-Export'
}




import bpy
import zero_length_zenith.importer
import zero_length_zenith.teximporter

def register():
    importer.register()
    teximporter.register()

def unregister():
    importer.unregister()
    teximporter.unregister()

if __name__ == "__main__":
    register()

