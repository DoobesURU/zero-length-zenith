# -*- coding: utf-8 -*-

"""
This file is part of ZLZ.

ZLZ is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ZLZ is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with ZLZ.  If not, see <https://www.gnu.org/licenses/>
"""


import bmesh, bpy


class TriangleWithMaterial:
    """Simple array of 3 vertex indices, along with a plasma material"""
    def __init__(self, vertexIndices, plMaterial):
        self.vertexIndices = vertexIndices
        self.plMaterial = plMaterial

def hasFlags(number, flags):
    return (number & flags) == flags

def stripIllegalChars(name):
    """Strips the string from illegal characters and common useless long strings. The returned name can be used as-is in both Unity and Unreal or even be used as a filename"""
    return name.replace("Material #", "m_") \
               .replace("_LIGHTMAPGEN", "_LM") \
               .replace(" ", "_") \
               .replace(".hsm", "") \
               .replace(".", "_") \
               .replace("~", "_") \
               .replace("#", "_") \
               .replace("*","_") \
               .replace("?","_") \
               .replace("\\","_") \
               .replace("/","_") \
               .replace("<","_") \
               .replace(">","_") \
               .replace(":","_") \
               .replace("é","e") \
               .replace("è","e") \
               .replace("ê","e") \
               .replace("à","a") \
               .replace("â","a") \
               .replace("ô","o") \
               .replace("î","i") \
               .replace("û","u") \
               .replace("ù","u") \
               .replace("ë","e") \
               .replace("ä","a") \
               .replace("ö","o") \
               .replace("ï","i") \
               .replace("ü","u") \
               .replace("ç","c") \
               .replace("&","_") \
               .replace("|","_") \
               .replace("ß","_")

def getColorArrayFromInt(col):
    """Returns (r, g, b, a) color from this ARGB int, in range [0:1]"""
    a = (col & 0xff000000) >> 24
    r = (col & 0xff0000) >> 16
    g = (col & 0xff00) >> 8
    b = col & 0xff
    return (r / 255, g / 255, b / 255, a / 255)

