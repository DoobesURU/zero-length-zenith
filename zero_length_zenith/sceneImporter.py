# -*- coding:utf-8 -*-

"""
This file is part of ZLZ.

ZLZ is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ZLZ is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with ZLZ.  If not, see <https://www.gnu.org/licenses/>
"""


"""
Handles importing pages with all their objects, etc.
Puts objects on both the main scene and separate per-page scenes.

Plasma is a bit trickier than Blender, because it has an entity+modifier -like structure. Hence, a Plasma sceneObject with multiple modifiers can't be mapped 1:1 to a Blender object, because Blender objects are of different types.
Because of this, we will sometime generate multiple Blender objects for one single Plasma sceneObject (all attached to a master object).

Note that we first import all PRPs before setting up parenting and references - see importer.py for details.
#"""

import bpy
from mathutils import *
from math import *
import PyHSPlasma as pl
from zero_length_zenith.utils import *

class SceneImporter:

    # layers on which we'll put different types of objects
    layerDefault        = (0,)
    layerColliders      = (1,)
    layerRegions        = (2,)
    layerAudio          = (3,)
    layerCameras        = (0, 4)
    layerSoftVolumes    = (5,)
    layerEmptiesMisc    = (6,)
    layerLights         = (0, 10)
    
    def __init__(self, parent):
        self.parent = parent
        self.bonesLength = .1 # display length of bones. Does not affect how the armature animates.
        self.mainScene = None
        self.curScene = None
        self.sceneNode = None
        self.sceneNodes = [] # sceneNodes are added to this list on each call to importScene
        self.currentlyImporting = False
        self.page = None
    
    def importScene(self, location):
        self.page = self.parent.rmgr.FindPage(location)
        print("\nImporting page %s" % self.page.page)
        self.sceneNode = self.parent.rmgr.getSceneNode(location)
        if not self.sceneNode or self.page.page in ["BuiltIn", "Textures"] or (self.sceneNode and len(self.sceneNode.sceneObjects) == 0 and len(self.sceneNode.poolObjects) == 0):
            print("    ...Does not contain visible data. Ignoring.")
            return
        print("    %s objects" % len(self.sceneNode.sceneObjects))
        
        self.sceneNodes.append(self.sceneNode)
        
        # import the whole Age into the current scene
        self.mainScene = bpy.context.scene
        self.mainScene.game_settings.material_mode = "GLSL"
        self.mainScene.name = self.page.age
        
        # ...but also import the page currently being processed in its own scene.
        self.curScene = bpy.data.scenes.new(self.page.page)
        self.curScene.game_settings.material_mode = "GLSL"
        self.curScene.render.engine = "PLASMA_GAME"
        self.mainScene.layers = self.curScene.layers = [True] * 20
        self.curScene.world = self.mainScene.world # copy the main scene's world
        self.curScene.frame_start = 0 # Plazma animations start at 0
        self.mainScene.frame_start = 0
        self.curScene.render.fps_base = self.mainScene.render.fps_base
        self.curScene.render.fps = self.mainScene.render.fps
        
        # setup the Korman page
        pageSettings = self.mainScene.world.plasma_age.pages
        kormanPage = pageSettings.add()
        kormanPage.name = self.page.page
        kormanPage.seq_suffix = location.page
        kormanPage.local_only = hasFlags(location.flags, pl.plLocation.kLocalOnly)
        # auto_load is set in the .age file, and can only be set by the importer calling us.
        
        # now, create and import all objects. References between objects won't be setup.
        self.currentlyImporting = True
        for sceneObjKey in self.sceneNode.sceneObjects:
            dataDict = self.parent.objectImporter.createObject(sceneObjKey)
        self.currentlyImporting = False
    
    def appendObjectToScenes(self, blObject, layerIds):
        """Link the given blObject to both the main scene, and the currently processed PRP's scene.
        The object will be put on the given layerIds on each scene."""
        if not self.currentlyImporting:
            raise RuntimeError("Cannot add object to scene: no scene currently processed.")
        layers = [False] * 20
        for id in layerIds:
            layers[id] = True
        self.mainScene.objects.link(blObject).layers = layers
        self.curScene.objects.link(blObject).layers = layers
        
        # also setup Korman object info
        plasmaObject = blObject.plasma_object
        plasmaObject.enabled = True
        plasmaObject.page = self.page.page
    
    def rebuildHierarchy(self):
        # parent objects (only doable once all objects are imported as object hierarchy is reversed in Blender)
        for childKey, parentKey in self.parent.objectParents.items():
            if not self.parent.validKey(parentKey):
                continue
            childObj = self.parent.getBlObjectFromKey(childKey)
            parentObj = self.parent.getBlObjectFromKey(parentKey)
            if not childObj or not parentObj or childObj.parent: # ignore if one of the object wasn't imported, or if the object already has a parent (armature)
                continue
            
            childObj.parent = parentObj

