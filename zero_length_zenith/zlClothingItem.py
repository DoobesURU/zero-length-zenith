# -*- coding:utf-8 -*-


"""
This file is part of ZLZ.

ZLZ is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ZLZ is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with ZLZ.  If not, see <https://www.gnu.org/licenses/>
"""


# ZLZ version of Plamza's plClothingItem class, since it doesn't have any binding in HSPlasma


import bpy
from mathutils import *
import PyHSPlasma as pl
from zero_length_zenith.utils import *

class zlClothingItem:
    
    class zlKey:
        def __init__(self):
            self.fFileOff = 0
            self.fObjSize = 0
        def read(self, stream):
            self.readUoid(stream)
            fFileOff = stream.readInt()
            fObjSize = stream.readInt()
        # def readUoid(self, stream):
            # contents = 0x1
            # location.read(S)
            # if hasFlags(contents, kHasLoadMask) && (!S->getVer().isNewPlasma() || S->getVer().isUniversal()):
                # loadMask.read(S)
            # else:
                # loadMask.setAlways()
            # classType = pdUnifiedTypeMap::PlasmaToMapped(S->readShort(), S->getVer())
            # if !S->getVer().isUruSP() && !S->getVer().isUniversal():
                # objID = S->readInt()
            # objName = S->readSafeStr()
            # if hasFlags(contents, kHasCloneIDs) && (S->getVer().isUru() || S->getVer().isUniversal()):
                # cloneID = S->readInt()
                # if (S->getVer() < MAKE_VERSION(2, 0, 57, 0)):
                    # clonePlayerID = 0
                # else:
                    # clonePlayerID = S->readInt()
            # else:
                # cloneID = clonePlayerID = 0
            # if hasFlags(contents, (kHasLoadMask | kHasLoadMask2)) && S->getVer().isNewPlasma():
                # loadMask.read(S)
    
    class SubElementInfo:
        def __init__(self):
            self.idx = 0
            self.k = plKey()
        def read(self, stream):
            self.idx = stream.readByte()
            self.k.read(stream)
    
    class ElementInfo:
        def __init__(self):
            self.elementname = ""
            self.count = 0
            self.textures = [] # SubElementInfo
        def read(self, stream):
            self.elementname = stream.readString()
            self.count = stream.readByte()
            for i in range(self.count):
                tex = SubElementInfo()
                tex.read(stream)
                self.textures.append(tex)
    
    def __init__(self):
        self.itemName = ""
        self.group = 0
        self.type = 0
        self.tileset = 0
        self.sortOrder = 0
        self.description = ""
        self.customText = ""
        self.hasIcon = 0
        self.icon = zlKey()
        self.elementCount = 0
        self.elements = []
        self.hasmesh1 = 0
        self.mesh1 = zlKey()
        self.hasmesh2 = 0
        self.mesh2 = zlKey()
        self.hasmesh3 = 0
        self.mesh3 = zlKey()
        self.accessory = zlKey()
        self.defaultTint11 = 0
        self.defaultTint21 = 0
        self.defaultTint12 = 0
        self.defaultTint22 = 0
        self.defaultTint13 = 0
        self.defaultTint23 = 0
    
    def read(self, stream):
        self.itemName = stream.readString()
        self.group = stream.readByte()
        self.type = stream.readByte()
        self.tileset = stream.readByte()
        self.sortOrder = stream.readByte()
        self.description = stream.readString()
        self.customText = stream.readString()
        self.hasIcon = stream.readByte()
        if (hasIcon)
            self.icon.read(stream)
        self.elementCount = stream.readInt()
        for i in range(self.elementCount):
            el = ElementInfo()
            el.read(stream)
            self.elements.append(el)
        self.hasmesh1 = stream.readByte()
        if (hasmesh1)
            self.mesh1.read(stream)
        self.hasmesh2 = stream.readByte()
        if (hasmesh2)
            self.mesh2.read(stream)
        self.hasmesh3 = stream.readByte()
        if (hasmesh3)
            self.mesh3.read(stream)
        self.accessory.read(stream)
        self.defaultTint11 = stream.readByte()
        self.defaultTint21 = stream.readByte()
        self.defaultTint12 = stream.readByte()
        self.defaultTint22 = stream.readByte()
        self.defaultTint13 = stream.readByte()
        self.defaultTint23 = stream.readByte()

