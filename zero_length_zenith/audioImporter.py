# -*- coding:utf-8 -*-

"""
This file is part of ZLZ.

ZLZ is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ZLZ is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with ZLZ.  If not, see <https://www.gnu.org/licenses/>
"""


"""
As strange as it sounds, this script handles importing sound emitters.
#"""

import bpy, os
from mathutils import *
from math import *
import PyHSPlasma as pl
from zero_length_zenith.sceneImporter import SceneImporter
from zero_length_zenith.utils import *

class AudioImporter:
    
    def __init__(self, parent):
        self.parent = parent
    
    def importAudio(self, sceneObjKey, audioKey):
        if not self.parent.validKey(audioKey):
            return None
        
        blObj = bpy.data.objects.new(sceneObjKey.name + "_SOUNDEMIT", None)
        self.parent.sceneImporter.appendObjectToScenes(blObj, SceneImporter.layerAudio)
        
        blObj.select = True
        bpy.context.scene.objects.active = blObj
        
        bpy.ops.object.plasma_modifier_add(types="soundemit")
        kormanAudio = blObj.plasma_modifiers.soundemit
        
        # a'ight. Let's fetch some actual win32streaming/static sounds and setup korman's sound object...
        for soundKey in audioKey.object.audible.object.sounds:
            plSound = soundKey.object
            kormanSound = kormanAudio.sounds.add()
            
            # also create a speaker object. Korman has no use for it and will not export it at all,
            # but it puts a pretty icon in the 3D view. Oh, and Blender will play the sound, assuming you're
            # rendering a video out of it from some reason...
            speaker = bpy.data.speakers.new(audioKey.name)
            blSubObj = bpy.data.objects.new(sceneObjKey.name + "_SPEAKER", speaker)
            self.parent.sceneImporter.appendObjectToScenes(blSubObj, SceneImporter.layerAudio)
            blSubObj.plasma_object.enabled = False # will never be exported anyway, might as well make it clear to the user...
            blSubObj.matrix_local = Matrix() # make sure it will stay at the parent's position
            blSubObj.parent = blObj
            
            # find the sound file itself.
            soundFilepath = os.path.join(self.parent.sfxFolderLocation, plSound.dataBuffer.object.fileName)
            # just like Korman, we must first check if it's already loaded somewhere. This is sound logic okay im out byyyye--
            for i in bpy.data.sounds:
                if soundFilepath == i.filepath:
                    sound = i
                    break
            else:
                sound = bpy.data.sounds.load(soundFilepath)
            kormanSound.sound = sound
            
            speaker.sound = sound
            speaker.muted = hasFlags(plSound.properties, pl.plWin32Sound.kPropFullyDisabled) or audioKey.object.getProperty(pl.plAudioInterface.kDisable)
            speaker.volume = plSound.desiredVolume
            speaker.volume_min = 0
            speaker.volume_max = 1
            speaker.distance_reference = plSound.minFalloff
            speaker.distance_max = plSound.maxFalloff
            speaker.cone_angle_inner = radians(plSound.innerCone)
            speaker.cone_angle_outer = radians(plSound.outerCone)
            speaker.cone_volume_outer = plSound.outerVol
            
            if hasFlags(plSound.properties, pl.plWin32Sound.kPropIs3DSound):
                # Korman restricts 3D-ness to the sound fx category. It makes sense, but is going to be a bit annoying for Cyan Ages.
                kormanSound.sfx_type = "kSoundFX"
            else:
                kormanSound.sfx_type = {
                    pl.plWin32Sound.kSoundFX: "kAmbience", # should be kSoundFX, but it's 3D, so set it as ambiance instead.
                    pl.plWin32Sound.kAmbience: "kAmbience",
                    pl.plWin32Sound.kBackgroundMusic: "kBackgroundMusic",
                    pl.plWin32Sound.kGUISound: "kGUISound",
                    pl.plWin32Sound.kNPCVoices: "kNPCVoices",
                    }[plSound.type]
            if hasFlags(plSound.properties, pl.plWin32Sound.kPropIs3DSound):
                kormanSound.channel = set("L" if plSound.channel == pl.plWin32Sound.kLeftChannel else "R")
            else:
                kormanSound.channel = set(("L", "R"))
            kormanSound.auto_start = hasFlags(plSound.properties, pl.plWin32Sound.kPropAutoStart)
            kormanSound.incidental = hasFlags(plSound.properties, pl.plWin32Sound.kPropIncidental)
            kormanSound.loop = hasFlags(plSound.properties, pl.plWin32Sound.kPropLooping)
            kormanSound.inner_cone = radians(plSound.innerCone)
            kormanSound.outer_cone = radians(plSound.outerCone)
            kormanSound.outside_volume = plSound.outerVol
            kormanSound.volume = plSound.desiredVolume * 100
            kormanSound.min_falloff = plSound.minFalloff
            kormanSound.max_falloff = plSound.maxFalloff
            for kFade, plFade in ((kormanSound.fade_in, plSound.fadeInParams), (kormanSound.fade_out, plSound.fadeOutParams)):
                if not plFade.lengthInSecs:
                    kFade.fade_type = "NONE"
                else:
                    kFade.fade_type = {
                        plSound.plFadeParams.kLinear: "kLinear",
                        plSound.plFadeParams.kLogarithmic: "kLogarithmic",
                        plSound.plFadeParams.kExponential: "kExponential",
                        }[plFade.type]
                    kFade.length = plFade.lengthInSecs
            
            # kormanSound.sfx_region = ??? # TODO
        
        blObj.select = False
        
        return blObj

